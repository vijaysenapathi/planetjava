/**
 * Created by vijaysenapathi on 10/02/16.
 */
public class NoClassDefFound {
    static class ErrorClass{
        ErrorClass(){
            throw new RuntimeException();
        }
    }

    public static void main(String[] args){
        ErrorClass ec = new NoClassDefFound.ErrorClass();
    }
}
