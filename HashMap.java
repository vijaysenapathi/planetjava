import javafx.util.Pair;

import java.util.Vector;

/**
 * Created by vijaysenapathi on 10/02/16. Creates a vector of vectors. The keys are hashed and based on the hash value placed in the corresponding position of the outer vector and objects with the same hash value are placed sequentially in the inner vector
 */
public class HashMap<K, V>{
    Vector< Vector <Pair<K,V> > > all;
    int N; //Current Capacity
    int M;//Number of pairs in the map currently
    private static final int initialCapacity = 16;
    private static final double loadFactor = 0.75;


    public HashMap(){
        N = initialCapacity;
        all = new Vector< Vector<Pair<K,V>> >();
        all.setSize(N);

        for(int i = 0;i<N;i++) {
            all.set(i, new Vector<Pair<K, V>>());
        }
        M = 0;
    }

    public void put(K k, V v){
        if (M < (N*loadFactor)){
            insertInWithoutRehashing(k,v);
        }else{
            doubleTheSizeAndRehash();
            insertInWithoutRehashing(k,v);
        }
        M++;
    }

    private void insertInWithoutRehashing(K k, V v){
        int pos = getHashValue(k);
        //checking if such a key already exists and skip the key if it exists
        Vector< Pair<K,V> > vc = all.get(pos);

        for(int i=0;i<vc.size();i++){
            if(vc.get(i).getKey().equals(k)){
                return;
            }
        }

        vc.add(new Pair<K, V>(k,v));

    }

    private void insertInWithoutRehashing(Pair<K,V> p){
        insertInWithoutRehashing(p.getKey(),p.getValue());
    }

    private void doubleTheSizeAndRehash(){
        Vector< Pair<K,V> > tempVector = new Vector< Pair<K,V> >();
        Vector< Pair<K,V> > innerVector;
        for(int i=0;i<N;i++){
            innerVector = all.get(i);
            for(int j=0;j<innerVector.size();j++){
                tempVector.add(innerVector.get(j));
            }
        }

        all.clear();
        N = N*2;
        all.setSize(N);

        for(int i=0;i<N;i++){
            all.set(i, new Vector< Pair<K,V> >());
        }

        for(int i=0;i<tempVector.size();i++) {
            insertInWithoutRehashing(tempVector.get(i));
        }
    }

    public V get(K k){
        int pos = getHashValue(k);
        Vector< Pair<K,V> > vc = all.get(pos);

        for(int i=0;i<vc.size() ;i++){
            if(vc.get(i).getKey().equals(k)){
                return vc.get(i).getValue();
            }
        }
        return null;
    }

    public void remove(K k){
        int pos = getHashValue(k);
        Vector< Pair<K,V> > vc = all.get(pos);

        for(int i=0;i<vc.size() ;i++){
            if(vc.get(i).getKey().equals(k)){
                vc.remove(i);
            }
        }
    }

    public void clear(){
        N = initialCapacity;
        all.clear();
        all.setSize(N);
        for(int i = 0;i<N;i++) {
            all.set(i, new Vector<Pair<K, V>>());
        }
    }

    @Override
    public String toString(){
        String ret = "";
        for(int i=0;i<all.size();i++){
            if(all.get(i).size() > 0){
                ret += i + ": ";
                for(int j=0;j<all.get(i).size();j++){
                    ret += all.get(i).get(j) + " ";
                }
                ret += "\n";
            }

        }
        return ret;
    }


    private int getHashValue(K k){
        return k.hashCode() % N;

    }

    public int getSize(){
        return N;
    }


    public static void main(String[] args){
        HashMap<String, Integer> hm = new HashMap<String, Integer>();
        for(int i=0;i<50;i++){
            hm.put("neo"+i,i);
            //System.out.println(hm.getSize());
        }

        System.out.println(hm);

        hm.remove("neo34");
        hm.remove("neo46");
        hm.remove("neo3");
        hm.remove("neo49");
        hm.remove("neo5");

        System.out.println("next: \n" + hm);

    }
}
